FROM node:10
ARG NODE_ENV
# Create app directory
WORKDIR /app
COPY package*.json ./
RUN npm install
# Bundle app source
COPY . .
EXPOSE 3000
ENTRYPOINT [ "node" ]
CMD ["index.js"]
var OrientDB = require('orientjs');
const RID = require('orientjs').RID;
const { Parser } = require('json2csv');
const createCsvWriter = require('csv-writer').createObjectCsvWriter;
const dateFormat = require('dateformat');
const path = require('path');
var cron = require('node-cron');
const gcsHelpers = require('./storage');
const bucketname = 'carrefour-data-drop-cdb';

const fields = ['user_id', 'my_club_member_id', 'first_name', 'last_name', 'email', 'primary_contact_number',
	'country', 'language', 'created_at', 'updated_at'];
const opts = { fields };

// email
// user_id
// my_club_member_id
// first_name
// last_name
// phone (format like 201207549986)
// country
// language
// marketing_preferences (if the user have subscribe or not to marketing)
// created_date_time – In GST for all countries. YYYYMMDD HH:MM:SS
// modified_date_time – In GST for all countries. YYYYMMDD HH:MM:SS
// event_type – signup or updated if everything is send in a file
var currentDate = new Date();
var yesterdayDate = new Date();
yesterdayDate.setDate(currentDate.getDate() - 1);
const fileName = 'user_' + dateFormat(currentDate, "yyyymmddHHMM") + '.csv';
const destinationPath = { destination: path.join('test', dateFormat(currentDate, "yyyy/mm/dd").toString(), fileName) }
const batchSize = 10000; // batch size 
const csvWriter = createCsvWriter({
	path: fileName,
	encoding: 'utf8',
	header: [
		{ id: 'email', title: 'email' },
		{ id: 'user_id', title: 'user_id' },
		{ id: 'my_club_member_id', title: 'my_club_member_id' },
		{ id: 'card_country', title: 'card_country' },
		{ id: 'first_name', title: 'first_name' },
		{ id: 'last_name', title: 'last_name' },
		{ id: 'primary_contact_number', title: 'phone' },
		{ id: 'country_of_operation', title: 'country' },
		{ id: 'preferred_language', title: 'language' },
		{ id: 'created_at', title: 'created_date_time' },
		{ id: 'updated_at', title: 'modified_date_time' },
		{ id: 'event_type', title: 'event_type' }
	]
});

var server = OrientDB({
	host: '10.71.4.18',
	port: 2424,
	username: 'root',
	password: 'orientdb'
});

var db = server.use({
	name: 'carrefour',
	username: 'matrix',
	password: 'Bainaizu2hai0ohRe3ai'

});

cron.schedule('0 */4 * * *', () => {
	console.log('Using Database:' + db.name);
	start = 0;
	end = 0;
	first = false

	async function writeData(records) {
		recordSet = [];
		records.forEach((item) => {
			try {
				i = 0;
				item.user_id = item.user_id.position;
				item.first_name = item.hasOwnProperty('first_name') ? item.first_name.trim() : "";
				item.last_name = item.hasOwnProperty('first_name') ? item.last_name.trim() : "";
				item.event_type = (item.hasOwnProperty('created_at') && item.hasOwnProperty('updated_at') && item.created_at.getTime() == item.updated_at.getTime() && item.created_at.getTime() > yesterdayDate.getTime) ? "New SignUp" : "Update";
				item.created_at = item.hasOwnProperty('created_at') ? dateFormat(item.created_at, "yyyymmdd hh:MM:ss") : "";
				item.updated_at = item.hasOwnProperty('updated_at') ? dateFormat(item.updated_at, "yyyymmdd hh:MM:ss") : "";
				if (item.hasOwnProperty('my_club_member_id')) {
					item.my_club_member_id.forEach((id) => {
						record = Object.assign({}, item);
						record.my_club_member_id = id;
						record.card_country = item.hasOwnProperty('card_country') ? item.card_country[i] : "";
						record.primary_contact_number = item.hasOwnProperty('primary_contact_number') ? item.primary_contact_number[i] : "";
						recordSet.push(record);
						i = i + 1;
					})
				} else {
					recordSet.push(item);
					console.log('writing record with out membership : ', recordSet.length)
				};
			} catch (err) {
				console.log(item);
				console.error(err);
			}
		});
		try {
			csvWriter.writeRecords(recordSet).then(() => console.log(recordSet.length, ' records were written successfully', Date()));
		} catch (err) {
			console.error(err);
		}
	}

	async function fetchData(db, resultset, start, end) {
		await db.select('@rid as user_id, out("hasLoyaltyCard").card_number AS my_club_member_id, out("hasLoyaltyCard").country_of_operation AS card_country,\
				first_name,last_name, username AS email, out("hasLoyaltyCard").primary_contact_number AS primary_contact_number, \
				country_of_operation, preferred_language,created_at,updated_at').from('customers').where('@rid BETWEEN :FROM AND :TO')
			.addParams({ FROM: new RID('#12:' + start), TO: new RID('#12:' + end) })
			.all().then(
				async function (select) {
					console.log('resultset count: ', select.length)
					await writeData(select);
				});
	}

	db.open().then(function () {
		resultset = [];
		var count = db.select('count(*)').from('customers').scalar();
		console.log('started: ', Date());
		count.then(async function (select) {
			console.log('Total Records: ', select)
			while (end < select) {
				start = end + 1;
				if (first) {
					start = 0;
				}
				end = end + batchSize;
				console.log('running query for start:', start, 'and end: ', end);
				var resultset = await fetchData(db, resultset, start, end);
				first = false;
			}
			console.log('file uploading ....');
			await gcsHelpers.copyFileToGCS(fileName, bucketname, destinationPath);
		});
		return resultset;
	}).then(function (result) {
		db.close().then(function () {
			console.log('closed');
		});
	});
	server.close();
});
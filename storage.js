const { Storage } = require('@google-cloud/storage');
const filesize = require('filesize');
const path = require('path');

const GOOGLE_CLOUD_PROJECT_ID = 'carrefour-main-account-221808'; // Replace with your project ID
const GOOGLE_CLOUD_KEYFILE = 'key.json'; // Replace with the path to the downloaded private key

const storage = new Storage({
   projectId: GOOGLE_CLOUD_PROJECT_ID,
   keyFilename: GOOGLE_CLOUD_KEYFILE,
});

/**
   * Get public URL of a file. The file must have public access
   * @param {string} bucketName
   * @param {Object} options
   * @return {string}
   */
exports.getFiles = async (bucketName, options) => {
   options = options || {};
   const [files] = await storage.bucket(bucketName).getFiles(options);
   if ( files.length > 0 ) {
   console.log( files.length, ' Files found.');
   files.forEach(file => {
      console.log('- ', file.name , ' file size : ', filesize(file.metadata.size) );
   });
   } else {
      console.log('0 Files Found.');
   }
};


/**
   * Copy file from local to a GCS bucket.
   * Uploaded file will be made publicly accessible.
   *
   * @param {string} localFilePath
   * @param {string} bucketName
   * @param {Object} [options]
   * @return {Promise.<string>} - The public URL of the uploaded file.
   */
exports.copyFileToGCS = (localFilePath, bucketName, options) => {
   options = options || {};

   const bucket = storage.bucket(bucketName);
   const fileName = path.basename(localFilePath);
   const file = bucket.file(fileName);

   return bucket.upload(localFilePath, options)
      .then(async function () {
         console.log("Verfiying file is loaded to bucket");
         await getFiles(bucketName, {prefix: options.destination});
      });
};